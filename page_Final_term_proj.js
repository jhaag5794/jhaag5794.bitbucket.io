var page_Final_term_proj =
[
    [ "Introduction", "page_Final_term_proj.html#sec_final_project_intro", null ],
    [ "Hardware", "page_Final_term_proj.html#sec_final_project_hardware", null ],
    [ "Balancing", "page_Final_term_proj.html#sec_final_project_balancing", null ],
    [ "Software", "page_Final_term_proj.html#sec_final_project_software", null ],
    [ "Tunning Controller", "page_Final_term_proj.html#sec_final_project_tunnig", [
      [ "Stable angle", "page_Final_term_proj.html#sec_final_project_tunnig_tilt_angle", null ],
      [ "Controll Parameters", "page_Final_term_proj.html#sec_final_project_tunnig_parameters", null ],
      [ "Problems", "page_Final_term_proj.html#sec_final_project_tunnig_problems", null ]
    ] ],
    [ "Remote Controll", "page_Final_term_proj.html#sec_final_project_remot_controll", null ],
    [ "Video of the project", "page_Final_term_proj.html#sec_final_video", null ],
    [ "Code Source Code", "page_Final_term_proj.html#sec_final_source", null ],
    [ "Software List", "page_software.html", "page_software" ]
];