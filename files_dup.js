var files_dup =
[
    [ "controller.py", "controller_8py.html", [
      [ "PID_controller", "classcontroller_1_1PID__controller.html", "classcontroller_1_1PID__controller" ]
    ] ],
    [ "IMU.py", "IMU_8py.html", [
      [ "IMU_communication", "classIMU_1_1IMU__communication.html", "classIMU_1_1IMU__communication" ]
    ] ],
    [ "IMU_Driver.py", "IMU__Driver_8py.html", "IMU__Driver_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "Motor.py", "Motor_8py.html", [
      [ "Robot_MotorDriver", "classMotor_1_1Robot__MotorDriver.html", "classMotor_1_1Robot__MotorDriver" ]
    ] ],
    [ "Motor_Controller.py", "Motor__Controller_8py.html", "Motor__Controller_8py" ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "Motor_Encoder.py", "Motor__Encoder_8py.html", "Motor__Encoder_8py" ]
];