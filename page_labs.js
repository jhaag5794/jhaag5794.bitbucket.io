var page_labs =
[
    [ "Lab1 - Motor Driver", "page_Lab1.html", [
      [ "Introduction", "page_Lab1.html#sec_Lab1_intro", null ],
      [ "Motor Driver", "page_Lab1.html#sec_Lab1_task", null ],
      [ "Source Code", "page_Lab1.html#sec_Lab1_code_source", null ]
    ] ],
    [ "Lab2 - Motor Encoder", "page_Lab2.html", [
      [ "Introduction", "page_Lab2.html#sec_Lab2_intro", null ],
      [ "Motor Encoder", "page_Lab2.html#sec_Lab2_task", null ],
      [ "Source Code", "page_Lab2.html#sec_Lab2_code_source", null ]
    ] ],
    [ "Lab3 - Motor Controller", "page_Lab3.html", [
      [ "Introduction", "page_Lab3.html#sec_Lab3_intro", null ],
      [ "Motor Controller", "page_Lab3.html#sec_Lab3_task", null ],
      [ "Tuning process", "page_Lab3.html#sec_Lab3_tunning", null ],
      [ "Conclusion", "page_Lab3.html#sec_Lab3_conclussion", null ],
      [ "Video Clip", "page_Lab3.html#sec_Lab3_video", null ],
      [ "Source Code", "page_Lab3.html#sec_Lab3_source_code", null ]
    ] ],
    [ "Lab4 - IMU Communication", "page_Lab4.html", [
      [ "Introduction", "page_Lab4.html#sec_Lab4_intro", null ],
      [ "IMU Testing", "page_Lab4.html#sec_Lab4_testing", null ],
      [ "Video Clip", "page_Lab4.html#sec_Lab4_video", null ],
      [ "Source Code", "page_Lab4.html#sec_Lab4_source_code", null ]
    ] ]
];