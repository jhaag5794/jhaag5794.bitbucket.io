var main_8py =
[
    [ "act_sig", "main_8py.html#a7e2bbc1e283e7ed108798903912a3fde", null ],
    [ "ang_vel", "main_8py.html#ac9c9037eabe3b36b44ab08723a54e18b", null ],
    [ "angle_offset", "main_8py.html#a4943a3ecee8707c93f8c72d8be080780", null ],
    [ "BNO055_I2C_ADDRESS", "main_8py.html#afa6bc96d95c8d0ef2cd6e32ecce6ce64", null ],
    [ "cal_stat", "main_8py.html#a6d762496280248c536172bb7a7203287", null ],
    [ "cur_time", "main_8py.html#a5b0f31bfb07b2235c54a119d0683255f", null ],
    [ "duty_threshold", "main_8py.html#a1887a8f854fbd82db2897945d66e9ad1", null ],
    [ "euler_angles", "main_8py.html#aafb8cc0275ceb47b27f17fc99569d661", null ],
    [ "imc", "main_8py.html#ab73cf69e08aadeaa6d3ea86e95fa917f", null ],
    [ "interval", "main_8py.html#a86060447ac22c1b649ef0497e8adf1ba", null ],
    [ "kd_gain", "main_8py.html#aa5b92759a6b4fe296292c5738522009e", null ],
    [ "ki_gain", "main_8py.html#aeb8a866a40f4ffcb1dbd93827844060a", null ],
    [ "kp_gain", "main_8py.html#a36a25064826a6fcf8c9057d333f0481d", null ],
    [ "moe_1", "main_8py.html#a1417f461660b25ac4cd162fc728aab60", null ],
    [ "moe_2", "main_8py.html#a313fff1ed54064fd2b3843d3074d2957", null ],
    [ "next_time", "main_8py.html#a97359d1c36ca6a548999bed09f02f11a", null ],
    [ "Operation_Mode", "main_8py.html#a6b8f10f1981947060ab667a21bc0ccd2", null ],
    [ "PID_con", "main_8py.html#a100afd35be3caad9231831a499b24380", null ],
    [ "sensor_calb", "main_8py.html#a358ccfdc573c875b7bd19a750ddc4943", null ],
    [ "wave_status", "main_8py.html#a6ebfdaa71adfa7d4ab026a1c240b3341", null ]
];