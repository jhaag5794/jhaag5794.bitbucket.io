var classMotor__Controller_1_1controller =
[
    [ "__init__", "classMotor__Controller_1_1controller.html#a4d1f6a80703ad6674704ee058d70a2d6", null ],
    [ "change_kp_gain", "classMotor__Controller_1_1controller.html#a5d073f48118e816e0622b8b90970b817", null ],
    [ "change_setposition", "classMotor__Controller_1_1controller.html#af43ce1c32169a7aa9ff94208bdf131d9", null ],
    [ "update", "classMotor__Controller_1_1controller.html#a64c7edbca426110252cb540cafc9f488", null ],
    [ "act_position", "classMotor__Controller_1_1controller.html#a280e4b2a94d1dd6d12c419daabb4d7bf", null ],
    [ "act_sig", "classMotor__Controller_1_1controller.html#a90380123495eba1c3c844f34de45e214", null ],
    [ "err_sig", "classMotor__Controller_1_1controller.html#a53dd59850dc1ecebf751a94f33ea95f0", null ],
    [ "kp_gain", "classMotor__Controller_1_1controller.html#a04af37efa489474192e61f5cf88d27e4", null ],
    [ "set_position", "classMotor__Controller_1_1controller.html#ae8c6aa86edcd8c5f4df7d23834ab6a92", null ]
];