var Motor__Controller_8py =
[
    [ "controller", "classMotor__Controller_1_1controller.html", "classMotor__Controller_1_1controller" ],
    [ "a", "Motor__Controller_8py.html#ac96bee0f60f18318cb2276848add0324", null ],
    [ "act_position", "Motor__Controller_8py.html#aea5425da56bed9d68d02da2d87ceb9e4", null ],
    [ "contr", "Motor__Controller_8py.html#a67d5d6c18d8defb20558172a0c98b3c7", null ],
    [ "contr_1", "Motor__Controller_8py.html#a216fa154764c2743bf47275a1f3a6358", null ],
    [ "diff", "Motor__Controller_8py.html#ae81a9e97f1c0fb201df73b532fba4a6e", null ],
    [ "duty", "Motor__Controller_8py.html#ace017eaaa1bc99f84201e65aa3fc9c2a", null ],
    [ "enco", "Motor__Controller_8py.html#a40d44edfd97bb9763884ad8f09f70185", null ],
    [ "idx", "Motor__Controller_8py.html#a32610f607ea64625be9f384169c964c2", null ],
    [ "kp_gain", "Motor__Controller_8py.html#abe145fc5637a24e788aff7e3bcd70608", null ],
    [ "length_ms", "Motor__Controller_8py.html#ad6bc59b0e8271a9b3a74ddc7ced921b7", null ],
    [ "moe", "Motor__Controller_8py.html#ae6a743b9f885003bcfd26dde88b1614d", null ],
    [ "position_var", "Motor__Controller_8py.html#a4cef6cbd77e86df4cd174d685827beda", null ],
    [ "start", "Motor__Controller_8py.html#a6d0946abdc6777030355af8326b5ebd0", null ],
    [ "target", "Motor__Controller_8py.html#ae196098f0fff8ea5fb9217da23eb4478", null ],
    [ "time_var", "Motor__Controller_8py.html#af14489f1d27dfd7a8a389d406ee55584", null ]
];