var annotated_dup =
[
    [ "controller", null, [
      [ "PID_controller", "classcontroller_1_1PID__controller.html", "classcontroller_1_1PID__controller" ]
    ] ],
    [ "IMU", null, [
      [ "IMU_communication", "classIMU_1_1IMU__communication.html", "classIMU_1_1IMU__communication" ]
    ] ],
    [ "IMU_Driver", null, [
      [ "IMU_driver", "classIMU__Driver_1_1IMU__driver.html", "classIMU__Driver_1_1IMU__driver" ]
    ] ],
    [ "Motor", null, [
      [ "Robot_MotorDriver", "classMotor_1_1Robot__MotorDriver.html", "classMotor_1_1Robot__MotorDriver" ]
    ] ],
    [ "Motor_Controller", null, [
      [ "controller", "classMotor__Controller_1_1controller.html", "classMotor__Controller_1_1controller" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "MotorDriver", "classMotor__Driver_1_1MotorDriver.html", "classMotor__Driver_1_1MotorDriver" ]
    ] ],
    [ "Motor_Encoder", null, [
      [ "Encoder", "classMotor__Encoder_1_1Encoder.html", "classMotor__Encoder_1_1Encoder" ]
    ] ]
];