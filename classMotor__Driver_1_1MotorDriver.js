var classMotor__Driver_1_1MotorDriver =
[
    [ "__init__", "classMotor__Driver_1_1MotorDriver.html#a65d926af9f175b49dd14c6b618eb6597", null ],
    [ "disable", "classMotor__Driver_1_1MotorDriver.html#abd4507fc3fdac3fc9a10b2d6fa7c6608", null ],
    [ "enable", "classMotor__Driver_1_1MotorDriver.html#af6576759bd1844dc919e0f3e7ca9c1f9", null ],
    [ "set_duty", "classMotor__Driver_1_1MotorDriver.html#a6900ec5e218f96f0bb2386dccf8a6c3e", null ],
    [ "EN_pin", "classMotor__Driver_1_1MotorDriver.html#a7da4800e83240c5f4467738fcb8d8009", null ],
    [ "IN1_pin", "classMotor__Driver_1_1MotorDriver.html#af40193d707228920a8dfe6a9c7ee3ec3", null ],
    [ "IN2_pin", "classMotor__Driver_1_1MotorDriver.html#a0b0aacd4e0cac3ac21f48e893c245639", null ],
    [ "tcha_1", "classMotor__Driver_1_1MotorDriver.html#ad595ed307bf165ee8211712ceff28ad8", null ],
    [ "tcha_2", "classMotor__Driver_1_1MotorDriver.html#a591815e9a494601482c66f4f38ec9b45", null ],
    [ "tim", "classMotor__Driver_1_1MotorDriver.html#a62a00b3480ed47b1e7b0a1f712dfcab6", null ]
];