var page_software =
[
    [ "Introduction", "page_software.html#sec_software_Intro", null ],
    [ "Main Function", "page_software.html#sec_software_Main", null ],
    [ "Classes", "page_software.html#sec_software_classes", null ],
    [ "Main script", "page_Main.html", [
      [ "Introduction", "page_Main.html#sec_Main_script_intro", null ],
      [ "Initializing", "page_Main.html#sec_Main_script_init", null ],
      [ "Balancing loop", "page_Main.html#sec_Main_script_Balancing", null ],
      [ "Detail Code Information", "page_Main.html#sec_Main_script_Detail", null ]
    ] ],
    [ "Motor_Driver", "page_Motor_Driver.html", [
      [ "Task", "page_Motor_Driver.html#sec_Motor_Driver_Task", null ],
      [ "Implementation", "page_Motor_Driver.html#sec_Motor_Driver_imp", null ]
    ] ],
    [ "PID_Controler", "page_PID_Controler.html", [
      [ "Task", "page_PID_Controler.html#sec_PID_Controler_Task", null ],
      [ "Implementation", "page_PID_Controler.html#sec_PID_Controler_imp", null ]
    ] ],
    [ "IMU_Communication", "page_IMU_com.html", [
      [ "Task", "page_IMU_com.html#sec_IMU_com_Task", null ],
      [ "Implementation", "page_IMU_com.html#sec_IMU_com_imp", null ]
    ] ]
];